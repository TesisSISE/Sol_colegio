package android.colegio.bean;

/**
 * Created by 33.1 on 26/04/2016.
 */
public class CriterioNota {

    private String codTipoEvaluacion;
    private String descripTipoEvaluacion;
    private double promedio;


    public String getCodTipoEvaluacion() {
        return codTipoEvaluacion;
    }

    public void setCodTipoEvaluacion(String codTipoEvaluacion) {
        this.codTipoEvaluacion = codTipoEvaluacion;
    }

    public String getDescripTipoEvaluacion() {
        return descripTipoEvaluacion;
    }

    public void setDescripTipoEvaluacion(String descripTipoEvaluacion) {
        this.descripTipoEvaluacion = descripTipoEvaluacion;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
}
