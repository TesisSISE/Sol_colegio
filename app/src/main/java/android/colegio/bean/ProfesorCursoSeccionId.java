package android.colegio.bean;

// Generated 07/04/2016 09:08:48 PM by Hibernate Tools 4.0.0

/**
 * ProfesorCursoSeccionId generated by hbm2java
 */
public class ProfesorCursoSeccionId implements java.io.Serializable {

	private String codProfesor;
	private String codCurso;
	private int idSeccion;

	public ProfesorCursoSeccionId() {
	}

	public ProfesorCursoSeccionId(String codProfesor, String codCurso,
			int idSeccion) {
		this.codProfesor = codProfesor;
		this.codCurso = codCurso;
		this.idSeccion = idSeccion;
	}

	public String getCodProfesor() {
		return this.codProfesor;
	}

	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}

	public String getCodCurso() {
		return this.codCurso;
	}

	public void setCodCurso(String codCurso) {
		this.codCurso = codCurso;
	}

	public int getIdSeccion() {
		return this.idSeccion;
	}

	public void setIdSeccion(int idSeccion) {
		this.idSeccion = idSeccion;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProfesorCursoSeccionId))
			return false;
		ProfesorCursoSeccionId castOther = (ProfesorCursoSeccionId) other;

		return ((this.getCodProfesor() == castOther.getCodProfesor()) || (this
				.getCodProfesor() != null && castOther.getCodProfesor() != null && this
				.getCodProfesor().equals(castOther.getCodProfesor())))
				&& ((this.getCodCurso() == castOther.getCodCurso()) || (this
						.getCodCurso() != null
						&& castOther.getCodCurso() != null && this
						.getCodCurso().equals(castOther.getCodCurso())))
				&& (this.getIdSeccion() == castOther.getIdSeccion());
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getCodProfesor() == null ? 0 : this.getCodProfesor()
						.hashCode());
		result = 37 * result
				+ (getCodCurso() == null ? 0 : this.getCodCurso().hashCode());
		result = 37 * result + this.getIdSeccion();
		return result;
	}

}
