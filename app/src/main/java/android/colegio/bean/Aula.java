package android.colegio.bean;

// Generated 07/04/2016 09:09:34 PM by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;

/**
 * Aula generated by hbm2java
 */
public class Aula implements java.io.Serializable {

	private String codAula;
	private Byte capacidad;
	private Byte vacantes;
	private String estadoAula;
	
	public Aula() {
	}

	public Aula(String codAula) {
		this.codAula = codAula;
	}

	public Aula(String codAula, Byte capacidad, Byte vacantes,
			String estadoAula) {
		this.codAula = codAula;
		this.capacidad = capacidad;
		this.vacantes = vacantes;
		this.estadoAula = estadoAula;
	}

	public String getCodAula() {
		return this.codAula;
	}

	public void setCodAula(String codAula) {
		this.codAula = codAula;
	}

	public Byte getCapacidad() {
		return this.capacidad;
	}

	public void setCapacidad(Byte capacidad) {
		this.capacidad = capacidad;
	}

	public Byte getVacantes() {
		return this.vacantes;
	}

	public void setVacantes(Byte vacantes) {
		this.vacantes = vacantes;
	}

	public String getEstadoAula() {
		return this.estadoAula;
	}

	public void setEstadoAula(String estadoAula) {
		this.estadoAula = estadoAula;
	}

}
