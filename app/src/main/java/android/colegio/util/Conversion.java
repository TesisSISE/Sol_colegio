package android.colegio.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;
import android.widget.ImageView;

public class Conversion {

    public byte[] hexStringToBinary(String s) {
        final int len = s.length();

        // "111" is not a valid hex encoding.
        if( len%2 != 0 )
            throw new IllegalArgumentException("hexBinary needs to be even-length: "+s);

        byte[] out = new byte[len/2];

        for( int i=0; i<len; i+=2 ) {
            int h = hexToBin(s.charAt(i  ));
            int l = hexToBin(s.charAt(i+1));
            if( h==-1 || l==-1 )
                throw new IllegalArgumentException("contains illegal character for hexBinary: "+s);

            out[i/2] = (byte)(h*16+l);
        }

        return out;
    }

    private static int hexToBin( char ch ) {
        if( '0'<=ch && ch<='9' )    return ch-'0';
        if( 'A'<=ch && ch<='F' )    return ch-'A'+10;
        if( 'a'<=ch && ch<='f' )    return ch-'a'+10;
        return -1;
    }

    private static final char[] hexCode = "0123456789ABCDEF".toCharArray();

    public String binaryToHexString(byte[] data) {
        StringBuilder r = new StringBuilder(data.length*2);
        for ( byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

    public byte[] decodeStringBase64(String s) {
        return Base64.decode(s, Base64.DEFAULT);
    }
    public String encodeStringBase64(byte[] b) {
        return Base64.encodeToString(b, Base64.DEFAULT);
    }


    public static byte[] getArrayBytesFromBitmap(Bitmap bmp) {
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bmp.compress(CompressFormat.PNG, 100, baos);
        byte[] array=baos.toByteArray();
        return array;
    }

    public static Bitmap getBitmapFromImageView(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        return bitmap;
    }

    public static byte[] getArrayBytesFromImageView(ImageView image) {
        Bitmap bmp=getBitmapFromImageView(image);
        byte[] blob=getArrayBytesFromBitmap(bmp);
        return blob;
    }

    public static void setImageBitmap(byte[] arrayBytes,ImageView img) {
        if(arrayBytes!=null && arrayBytes.length>0) {
            ByteArrayInputStream imageStream=new ByteArrayInputStream(arrayBytes);
            Bitmap bmp=BitmapFactory.decodeStream(imageStream);
            img.setImageBitmap(bmp);
        }
    }


    public static String quitarTildes(String s) {
        String ss=s;
        Map<String, String> map=new HashMap<String, String>();
        map.put("á", "a");
        map.put("é", "e");
        map.put("í", "i");
        map.put("ó", "o");
        map.put("ú", "u");
        map.put("ñ", "nn");
        map.put("¡", "!");

        for(Map.Entry<String, String> entry:map.entrySet()) {
            String key=entry.getKey();
            String value=entry.getValue();

            ss=ss.replaceAll(key, value);
            if(!key.equals("¡")) {
                ss=ss.replaceAll(key.toUpperCase(), value.toUpperCase());
            }
        }
        return ss;
    }


}