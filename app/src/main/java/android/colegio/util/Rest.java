package android.colegio.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;

import principal.colegio.android.sol_colegio.R;

/**
 * Created by 33.1 on 15/04/2016.
 */
public final class Rest {

    private static final String IP;
    public static final String URL_SERVICE_BASE;
    public static final String URL_HTTP_IMAGENES;

    private static final String USERNAME_FTP;
    private static final String PASSWORD_FTP;
    private static final int PORT_FTP;

    static {
		IP="190.117.118.40";
        //IP = "192.168.1.240";
        int PORT_HTTP = 4444;
        String URL_BASE = "http://"+IP+":"+Integer.toString(PORT_HTTP);
        URL_SERVICE_BASE = URL_BASE+"/WSColegio/rest";
        URL_HTTP_IMAGENES = URL_BASE+"/WSColegio/imagenes/";

        USERNAME_FTP="movil";
        PASSWORD_FTP="titulacion";
        PORT_FTP = 21;
    }

    public static void addParametersToPath(StringBuilder pathReal, Map<String,String> params){
        int total=params.size();
        int i=1;
        String key=null;
        String value=null;
        for (Map.Entry<String,String> entry : params.entrySet()) {
            if(i==1) pathReal.append("?");
            key = entry.getKey();
            value = entry.getValue();
            pathReal.append(key+"="+value);
            if(i!=total) pathReal.append("&");
            i++;
        }
    }

    public static void descargarImagen(String url, final Context context, final ImageView imageView){
        Glide
                .with(context)
                .load(url)
                .asBitmap()
                .centerCrop()
                .placeholder(R.drawable.persona)
                .into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }


    public static void descargarImagenPorFTP(final String fileRelativePath, final Context context, final ImageView imageView){
        new DownloadTask(context, imageView).execute(fileRelativePath);
    }


    static class DownloadTask extends AsyncTask<String, Void, File>{
        private ImageView imageView;
        private Context context;

        public DownloadTask(Context context, ImageView imageView){
            this.context = context;
            this.imageView = imageView;
        }

        @Override
        protected File doInBackground(String... params) {
            String fileRelativePath = params[0];
            File file = new File(context.getCacheDir(), fileRelativePath);

            if (!file.exists() || (file.exists() & file.length()==0)) {
                String[] split = fileRelativePath.split("/");
                File directory = new File(context.getCacheDir()+"/"+split[0]);

                if(!directory.exists()){
                    if(directory.mkdir()){
                        Log.d("FILE", "Directorio creado");
                    }else{
                        Log.d("FILE", "No se pudo crear el directorio");
                    }
                }

                    FTPClient ftpClient = new FTPClient();
                    FileOutputStream fileOutputStream = null;
                    try {
                        fileOutputStream = new FileOutputStream(file);
                        ftpClient = new FTPClient();
                        ftpClient.connect(IP, PORT_FTP);

                        if (FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                            ftpClient.login(USERNAME_FTP, PASSWORD_FTP);
                            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                            ftpClient.enterLocalPassiveMode();

                            ftpClient.retrieveFile("/imagenes/"+fileRelativePath, fileOutputStream);
                            fileOutputStream.close();
                            ftpClient.disconnect();
                        }

                    } catch (SocketException e) {
                        Log.e("FTP TEST", "SocketException: " + e.getStackTrace().toString() + " " + e.getMessage());
                    } catch (UnknownHostException e) {
                        Log.e("FTP TEST", "UnknownHostException: " + e.getStackTrace().toString() + " " + e.getMessage());
                    } catch (FileNotFoundException e) {
                        Log.e("FTP TEST", "UnknownHostException: " + e.getStackTrace().toString() + " " + e.getMessage());
                        e.printStackTrace();
                    } catch (IOException e) {
                        Log.e("FTP TEST", "IOException: " + e.getStackTrace().toString() + " " + e.getMessage());
                    }

            }

            return file;
        }

        @Override
        protected void onPostExecute(File file) {
            if (file.exists()) {
                Glide
                        .with(context)
                        .load(file)
                        .asBitmap()
                        .centerCrop()
                        .placeholder(R.drawable.persona)
                        .into(new BitmapImageViewTarget(imageView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                imageView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            }
        }
    }
}