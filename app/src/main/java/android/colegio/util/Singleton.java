package android.colegio.util;

import android.colegio.bean.Estudiante;
import android.colegio.bean.Usuario;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by kael on 14/04/2016.
 */
public class Singleton {
    private static Singleton instance;
    private Usuario usuario;
    private Estudiante estudiante;
    private Context context;

    private Singleton() {

    }

    public static Singleton getInstance() {
        if (instance == null) instance = new Singleton();
        return instance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public void setContext(Context context) {
        this.context = context.getApplicationContext();
    }


    public void logout() {
        usuario = null;
        estudiante = null;
    }
}