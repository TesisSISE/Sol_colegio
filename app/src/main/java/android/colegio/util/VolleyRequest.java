package android.colegio.util;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by 33.1 on 03/05/2016.
 */
public class VolleyRequest {
    private static VolleyRequest instance;
    private RequestQueue requestQueue;
    private static Context context;
    private int timeOut;


    private VolleyRequest(Context context){
        this.context = context.getApplicationContext();
        requestQueue = getRequestQueue();
        timeOut = 10000; //5 segundos
    }

    public static synchronized VolleyRequest getInstance(Context context){
        if(instance==null) instance = new VolleyRequest(context);
        return instance;
    }


    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public  void addToRequestQueue(Request request) {
        getRequestQueue().add(request);
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
