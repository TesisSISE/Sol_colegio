package android.colegio.adapter;

import android.colegio.bean.Evaluacion;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import principal.colegio.android.sol_colegio.R;

/**
 * Created by 33.1 on 25/05/2016.
 */
public class EvaluacionAdapter extends RecyclerView.Adapter<EvaluacionAdapter.EvaluacionViewHolder> {

    private List<Evaluacion> lista;
    private Context context;
    private DecimalFormat formatter;

    public EvaluacionAdapter(List<Evaluacion> lista) {
        this.lista = lista;
        formatter=new DecimalFormat("##00");
    }

    public Evaluacion getEvaluacion(int position){
        return lista.get(position);
    }

    @Override
    public EvaluacionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.nota_card, parent, false);
        return new EvaluacionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EvaluacionViewHolder holder, int position) {
        Evaluacion evaluacion = getEvaluacion(position);
        holder.lblNota.setText(evaluacion.getDescripEvaluacion()+": "+formatter.format(evaluacion.getNota()));
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }



    public class EvaluacionViewHolder extends RecyclerView.ViewHolder{
        public TextView lblNota;

        public EvaluacionViewHolder(View v) {
            super(v);
            lblNota = (TextView) v.findViewById(R.id.lblNota_card);
        }
    }
}
