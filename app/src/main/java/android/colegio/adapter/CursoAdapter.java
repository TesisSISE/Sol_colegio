package android.colegio.adapter;

import android.colegio.bean.Curso;
import android.colegio.util.Rest;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import principal.colegio.android.sol_colegio.R;

public class CursoAdapter extends RecyclerView.Adapter<CursoAdapter.CursoViewHolder> {
    private List<Curso> listaCursos;
    private Context context;
    private OnItemClickListener clickListener;


    public CursoAdapter(List<Curso> listaCursos){
        this.listaCursos = listaCursos;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener){
        this.clickListener =  clickListener;
    }

    public Curso getCurso(int position){
        return listaCursos.get(position);
    }


	@Override
	public CursoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.curso_card, parent, false);
        return new CursoViewHolder(v);
	}

	@Override
	public void onBindViewHolder(final CursoViewHolder holder, int position) {
        Curso curso = listaCursos.get(position);
        String url = Rest.URL_HTTP_IMAGENES+curso.getImagenCurso();
        holder.lblNombre.setText(curso.getDescripCurso());
        Rest.descargarImagen(url, context, holder.imgCurso);
	}

	@Override
	public int getItemCount() {
		return listaCursos.size();
	}

	public class CursoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		public ImageView imgCurso;
        public TextView lblNombre;

        public CursoViewHolder(View v){
            super(v);
            imgCurso = (ImageView) v.findViewById(R.id.imagen);
            lblNombre = (TextView) v.findViewById(R.id.nombre);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(this);
        }
    }


    public interface OnItemClickListener{
        void onClick(RecyclerView.ViewHolder v);
    }



}