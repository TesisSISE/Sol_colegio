package android.colegio.adapter;

import android.colegio.bean.Seccion;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import principal.colegio.android.sol_colegio.R;

/**
 * Created by 33.1 on 03/06/2016.
 */
public class SeccionAdapter extends RecyclerView.Adapter<SeccionAdapter.SeccionViewHolder> {
    private List<Seccion> lista;
    private Context context;
    private OnItemClickListener clickListener;

    public SeccionAdapter(List<Seccion> lista) {
        this.lista = lista;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener){
        this.clickListener =  clickListener;
    }

    public Seccion getSeccion(int position){
        return lista.get(position);
    }

    @Override
    public SeccionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(context).inflate(R.layout.seccion_card, parent, false);
        return new SeccionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SeccionViewHolder holder, int position) {
        Seccion seccion = lista.get(position);
        holder.lblGradoSeccion.setText(seccion.getGradoSeccion());
        holder.lblTurno.setText(seccion.getTurno());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class SeccionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView lblGradoSeccion;
        public TextView lblTurno;

        public SeccionViewHolder(View itemView) {
            super(itemView);
            lblGradoSeccion = (TextView) itemView.findViewById(R.id.lblGradoSeccion);
            lblTurno = (TextView) itemView.findViewById(R.id.lblTurno);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(this);
        }
    }
    public interface OnItemClickListener{
        void onClick(RecyclerView.ViewHolder v);
    }
}
