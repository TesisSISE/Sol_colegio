package android.colegio.adapter;

import android.colegio.util.Rest;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import principal.colegio.android.sol_colegio.R;

/**
 * Created by 33.1 on 20/04/2016.
 */
public class RecordAcademicoAdapter extends BaseAdapter{

    private ArrayList<Hashtable<String, Object>> lista;
    private LayoutInflater inflater;
    private DecimalFormat formatter;


    public RecordAcademicoAdapter(Context context, ArrayList<Hashtable<String, Object>> lista){
        this.inflater = inflater.from(context);
        this.lista = lista;
        formatter=new DecimalFormat("##00");
    }


    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).hashCode();
    }

    private static class Control{
        ImageView imgCurso;
        TextView lblCurso;
        TextView lblNota1;
        TextView lblNota2;
        TextView lblNota3;
    }



    @Override
    public View getView(int position, View v, ViewGroup parent) {
        Control control;

        if (v == null) {
            v = inflater.inflate(R.layout.grilla_record_academico, null);
            control = new Control();
            control.imgCurso = (ImageView) v.findViewById(R.id.imgCurso);
            control.lblCurso = (TextView) v.findViewById(R.id.lblCurso);
            control.lblNota1 = (TextView) v.findViewById(R.id.lblNota1);
            control.lblNota2 = (TextView) v.findViewById(R.id.lblNota2);
            control.lblNota3 = (TextView) v.findViewById(R.id.lblNota3);
            v.setTag(control);
        }else{
            control = (Control) v.getTag();
        }

        Hashtable<String, Object> curso = lista.get(position);

        String url = Rest.URL_HTTP_IMAGENES+curso.get("ImagenCurso").toString();

        control.lblCurso.setText(curso.get("DescripCurso").toString());
        control.lblNota1.setText(formatter.format(curso.get("Primer")));
        control.lblNota2.setText(formatter.format(curso.get("Segundo")));
        control.lblNota3.setText(formatter.format(curso.get("Tercero")));
        Rest.descargarImagen(url, inflater.getContext(), control.imgCurso);
        return v;
    }

}
