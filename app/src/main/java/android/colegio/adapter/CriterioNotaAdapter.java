package android.colegio.adapter;

import android.colegio.bean.CriterioNota;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.text.DecimalFormat;
import java.util.ArrayList;

import principal.colegio.android.sol_colegio.R;

/**
 * Created by 33.1 on 26/04/2016.
 */
public class CriterioNotaAdapter extends BaseAdapter{
    private ArrayList<CriterioNota> lista;
    private LayoutInflater inflater;
    private DecimalFormat formatter;


    public CriterioNotaAdapter(Context context, ArrayList<CriterioNota> lista){
        this.inflater = inflater.from(context);
        this.lista = lista;
        formatter=new DecimalFormat("##00");
    }



    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).hashCode();
    }

    private static class Control{
        TextView lblDescripcion;
        TextView lblNota;
    }



    @Override
    public View getView(int position, View v, ViewGroup parent) {
        Control control;

        if (v == null) {
            v = inflater.inflate(R.layout.grilla_trimestrenota, null);
            control = new Control();
            control.lblDescripcion = (TextView) v.findViewById(R.id.lblDescripcion);
            control.lblNota = (TextView) v.findViewById(R.id.lblNota);

            v.setTag(control);
        }else{
            control = (Control) v.getTag();
        }

        CriterioNota criterioNota = lista.get(position);

        control.lblDescripcion.setText(criterioNota.getDescripTipoEvaluacion());
        control.lblNota.setText(formatter.format(criterioNota.getPromedio()));


        return v;
    }
}
