package android.colegio.adapter;

import java.util.ArrayList;

import android.colegio.bean.Profesor;
import principal.colegio.android.sol_colegio.R;

import android.colegio.util.Rest;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ProfesorAdapter extends BaseAdapter {


	private ArrayList<Profesor>  lista;
	private LayoutInflater inflater;
	
	public ProfesorAdapter(Context contexto, ArrayList<Profesor> lista){
		this.inflater = inflater.from(contexto);
		this.lista=lista;
	}

	@Override
	public int getCount() 
	{	return  lista.size() ;
	}

	@Override
	public Object getItem(int position)
	{	return    lista.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return  lista.get(position).hashCode();
	}
	
	private static  class Control
	{
		TextView  LBLNOMBRE;
		ImageView IMAGEN;
	}
	
	@Override
	public View getView(int position, View v, ViewGroup parent) {
	
		Control control;
		
		if(v==null)
		{
			v= inflater.inflate(R.layout.grilla_profesor,null);
			control=new Control();
			control.LBLNOMBRE = (TextView)v.findViewById(R.id.LBLNOMBREPROFESOR);
			control.IMAGEN = (ImageView)v.findViewById(R.id.IMAGEN);
			v.setTag(control);
		}
		else
		{
			control =(Control)v.getTag();
		}

		Profesor profesor = lista.get(position);

		String url = Rest.URL_HTTP_IMAGENES+profesor.getFotoProfesor();
		String apellidoCompleto = profesor.getApePaternoProfesor() + " " + profesor.getApeMaternoProfesor();
		control.LBLNOMBRE.setText(profesor.getNomProfesor()+" "+apellidoCompleto);
		Rest.descargarImagen(url, inflater.getContext(), control.IMAGEN);
		return  v;
	}
}