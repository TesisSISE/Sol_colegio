package android.colegio.dao;

import android.colegio.bean.MessageDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by 33.1 on 03/05/2016.
 */
public class ErrorVolley implements Response.ErrorListener {
    private Context context;
    private MessageDialog dialog;

    public ErrorVolley(Context context){
        this.context = context;
    }

    public ErrorVolley(Context context, MessageDialog dialog){
        this.context = context;
        this.dialog = dialog;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(dialog!=null) dialog.closeProgressBar();
        if (error instanceof NoConnectionError) {
            Toast.makeText(context, "No se pudo establecer la conexion: "+error, Toast.LENGTH_LONG).show();
        }else if(error instanceof ParseError){
            Toast.makeText(context, "Usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(context, "Error : "+error, Toast.LENGTH_LONG).show();
        }
        Log.e("ERROR", "onErrorResponse " + error);

    }
}
