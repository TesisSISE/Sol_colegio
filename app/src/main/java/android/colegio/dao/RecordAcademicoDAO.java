package android.colegio.dao;


import android.content.Context;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by 33.1 on 21/04/2016.
 */
public class RecordAcademicoDAO {
    private Context context;

    public RecordAcademicoDAO(Context context) {
        this.context = context;
    }


    public ArrayList<Hashtable<String, Object>> parseJSON(JSONArray jsonArray){
        ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();

        try {
            JSONObject jsonObject = null;

            Hashtable<String, Object> curso = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                curso = new Hashtable<String, Object>();
                curso.put("DescripCurso",jsonObject.getString("DescripCurso"));
                curso.put("ImagenCurso",jsonObject.getString("ImagenCurso"));
                curso.put("Primer",jsonObject.getDouble("Primer"));
                curso.put("Segundo",jsonObject.getDouble("Segundo"));
                curso.put("Tercero",jsonObject.getDouble("Tercero"));
                curso.put("PromedioFinal",jsonObject.getDouble("PromedioFinal"));
                lista.add(curso);
            }


        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();

        }

        return lista;
    }

}
