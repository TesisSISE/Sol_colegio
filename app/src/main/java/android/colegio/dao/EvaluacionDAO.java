package android.colegio.dao;

import android.colegio.bean.CriterioNota;
import android.colegio.bean.Evaluacion;
import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 33.1 on 25/05/2016.
 */
public class EvaluacionDAO {
    private Context context;

    public EvaluacionDAO(Context context) {
        this.context = context;
    }

    public ArrayList<Evaluacion> parse(JSONArray jsonArray){
        ArrayList<Evaluacion> lista = new ArrayList<Evaluacion>();
        Evaluacion evaluacion = null;

        try {
            JSONObject jsonObject = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                evaluacion = new Evaluacion();
                evaluacion.setIdEvaluacion(jsonObject.getInt("IdEvaluacion"));
                evaluacion.setDescripEvaluacion(jsonObject.getString("DescripEvaluacion"));
                evaluacion.setNota((byte)jsonObject.getInt("nota"));
                lista.add(evaluacion);
            }
        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return lista;
    }
}
