package android.colegio.dao;

import android.colegio.bean.Profesor;
import android.content.Context;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by hramos on 8/04/2016.
 */
public class ProfesorDAO {
    private Context context;
    
    public ProfesorDAO(Context context){
        this.context = context;
    }

    public ArrayList<Profesor> parseJSON(String json){
        ArrayList<Profesor> listaProfesores = new ArrayList<Profesor>();

        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = null;
            Profesor profesor = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                profesor = new Profesor();
                profesor.setCodProfesor(jsonObject.getString("CodProfesor"));
                profesor.setNomProfesor(jsonObject.getString("NomProfesor"));
                profesor.setApePaternoProfesor(jsonObject.getString("ApePaternoProfesor"));
                profesor.setApeMaternoProfesor(jsonObject.getString("ApeMaternoProfesor"));
                profesor.setSexoProfesor(jsonObject.getString("SexoProfesor").charAt(0));
                profesor.setEmailProfesor(jsonObject.getString("EmailProfesor"));
                profesor.setTelfProfesor(jsonObject.getString("TelfProfesor"));
                profesor.setFotoProfesor(jsonObject.getString("FotoProfesor"));

                listaProfesores.add(profesor);
            }


        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return listaProfesores;
    }
}
