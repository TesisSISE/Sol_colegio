package android.colegio.dao;

import android.colegio.bean.Curso;
import android.content.Context;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hramos on 17/04/2016.
 */
public class CursoDAO {
    private Context context;

    public CursoDAO(Context context){
        this.context = context;
    }

    public ArrayList<Curso> parseJSON(JSONArray jsonArray){
        ArrayList<Curso> listaCursos = new ArrayList<Curso>();

        try {
            JSONObject jsonObject = null;
            Curso curso = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                curso = new Curso();
                curso.setImagenCurso(jsonObject.getString("ImagenCurso"));
                curso.setCodCurso(jsonObject.getString("CodCurso"));
                curso.setDescripCurso(jsonObject.getString("DescripCurso"));
                listaCursos.add(curso);
            }

        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return listaCursos;
    }
}