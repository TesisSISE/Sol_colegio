package android.colegio.dao;

import android.colegio.bean.CriterioNota;
import android.content.Context;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by 33.1 on 26/04/2016.
 */
public class CriterioNotaDAO {
    private Context context;

    public CriterioNotaDAO(Context context) {
        this.context = context;
    }

    public ArrayList<CriterioNota> parseJSON(JSONArray jsonArray){
        ArrayList<CriterioNota> lista = new ArrayList<CriterioNota>();
        CriterioNota criterioNota = null;

        try {
            JSONObject jsonObject = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                criterioNota = new CriterioNota();
                criterioNota.setCodTipoEvaluacion(jsonObject.getString("CodTipoEvaluacion"));
                criterioNota.setDescripTipoEvaluacion(jsonObject.getString("DescripTipoEvaluacion"));
                criterioNota.setPromedio(jsonObject.getDouble("Promedio"));
                lista.add(criterioNota);
            }
        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }


        return lista;
    }
}
