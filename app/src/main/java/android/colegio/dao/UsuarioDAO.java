package android.colegio.dao;

import android.colegio.bean.Apoderado;
import android.colegio.bean.Director;
import android.colegio.bean.Profesor;
import android.colegio.bean.TipoUsuario;
import android.colegio.bean.Usuario;
import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by hramos on 2/04/2016.
 */
public class UsuarioDAO {
    private Context context;

    public UsuarioDAO(Context context){
        this.context = context;
    }


    public Usuario parseJSON(JSONObject jsonobject){
        Usuario usuario = null;
        try {

            if(jsonobject!=null && jsonobject.length()>0){
                usuario = new Usuario();
                usuario.setCodUsuario(jsonobject.getString("codUsuario"));
                //tipo usuario
                JSONObject jsonTipoUsuario = jsonobject.getJSONObject("tipoUsuario");

                TipoUsuario tipoUsuario = new TipoUsuario();
                tipoUsuario.setCodTipoUsuario(jsonTipoUsuario.getString("codTipoUsuario"));
                tipoUsuario.setDescripTipoUsuario(jsonTipoUsuario.getString("descripTipoUsuario"));

                usuario.setTipoUsuario(tipoUsuario);

                usuario.setNomUsuario(jsonobject.getString("nomUsuario"));
                usuario.setClaveUsuario(jsonobject.getString("claveUsuario"));
                usuario.setEstadoUsuario(jsonobject.getString("estadoUsuario"));
                //TIPO DE OBJETO DE USUARIO (PROFESOR, DIRECTOR O APODERADO)

                if(tipoUsuario.getDescripTipoUsuario().equalsIgnoreCase("Profesor")){
                    JSONObject jsonProfesor = jsonobject.getJSONObject("profesor");
                    Profesor profesor = new Profesor();
                    profesor.setCodProfesor(jsonProfesor.getString("codProfesor"));
                    profesor.setNomProfesor(jsonProfesor.getString("nomProfesor"));
                    profesor.setApePaternoProfesor(jsonProfesor.getString("apePaternoProfesor"));
                    profesor.setApeMaternoProfesor(jsonProfesor.getString("apeMaternoProfesor"));
                    profesor.setSexoProfesor(jsonProfesor.getString("sexoProfesor").charAt(0));
                    profesor.setDirProfesor(jsonProfesor.getString("dirProfesor"));
                    profesor.setEmailProfesor(jsonProfesor.getString("emailProfesor"));
                    profesor.setTelfProfesor(jsonProfesor.getString("telfProfesor"));
                    //profesor.setFechaNacProfesor(new Date(jsonProfesor.getString("fechaNacProfesor")));

                    usuario.setProfesor(profesor);
                }
                else if(tipoUsuario.getDescripTipoUsuario().equalsIgnoreCase("Director")){
                    JSONObject jsonDirector = jsonobject.getJSONObject("director");
                    Director director = new Director();
                    director.setCodDirector(jsonDirector.getString("codDirector"));
                    director.setNomDirector(jsonDirector.getString("nomDirector"));
                    director.setApePaternoDirector(jsonDirector.getString("apePaternoDirector"));
                    director.setApeMaternoDirector(jsonDirector.getString("apeMaternoDirector"));
                    director.setDniDirector(jsonDirector.getString("dniDirector"));
                    director.setSexoDirector(jsonDirector.getString("sexoDirector").charAt(0));

                    usuario.setDirector(director);
                }
                else if(tipoUsuario.getDescripTipoUsuario().equalsIgnoreCase("Apoderado")){
                    JSONObject jsonApoderado = jsonobject.getJSONObject("apoderado");
                    Apoderado apoderado = new Apoderado();
                    apoderado.setCodApoderado(jsonApoderado.getString("codApoderado"));
                    apoderado.setNomApoderado(jsonApoderado.getString("nomApoderado"));
                    apoderado.setApePaternoApoderado(jsonApoderado.getString("apePaternoApoderado"));
                    apoderado.setApeMaternoApoderado(jsonApoderado.getString("apeMaternoApoderado"));
                    apoderado.setDirApoderado(jsonApoderado.getString("dirApoderado"));
                    apoderado.setTelfApoderado(jsonApoderado.getString("telfApoderado"));
                    apoderado.setDniApoderado(jsonApoderado.getString("dniApoderado"));
                    apoderado.setSexoApoderado(jsonApoderado.getString("sexoApoderado").charAt(0));
                    apoderado.setFotoApoderado(jsonApoderado.getString("fotoApoderado"));
                    usuario.setApoderado(apoderado);
                }
            }

        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return usuario;
    }

    public String getResultado(JSONObject jsonobject){
        String resultado = null;
        try {
            if(jsonobject!=null && jsonobject.length()>0){
                resultado = jsonobject.getString("resultado");
            }
        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
        return resultado;
    }
}
