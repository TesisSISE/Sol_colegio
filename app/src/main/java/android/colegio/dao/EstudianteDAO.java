package android.colegio.dao;

import android.colegio.bean.Estudiante;
import android.content.Context;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by hramos on 16/04/2016.
 */
public class EstudianteDAO {
    private Context context;

    public EstudianteDAO(Context context){
        this.context = context;
    }

    public ArrayList<Estudiante> parseJSON(JSONArray jsonArray){
        ArrayList<Estudiante> listaEstudiante = new ArrayList<Estudiante>();

        try {
            JSONObject jsonObject = null;
            Estudiante estudiante = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                estudiante = new Estudiante();
                estudiante.setCodEstudiante(jsonObject.getString("CodEstudiante"));
                estudiante.setNomEstudiante(jsonObject.getString("NomEstudiante"));
                estudiante.setApePaternoEstudiante(jsonObject.getString("ApePaternoEstudiante"));
                estudiante.setApeMaternoEstudiante(jsonObject.getString("ApeMaternoEstudiante"));
                estudiante.setSexoEstudiante(jsonObject.getString("SexoEstudiante").charAt(0));
                estudiante.setDniEstudiante(jsonObject.getString("DniEstudiante"));
                //estudiante.setApePaternoEstudiante(jsonObject.getString("Parentesco"));

                listaEstudiante.add(estudiante);
            }


        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return listaEstudiante;
    }

}