package android.colegio.dao;

import android.colegio.bean.Curso;
import android.colegio.bean.Seccion;
import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by 33.1 on 03/06/2016.
 */
public class SeccionDAO {
    private Context context;

    public SeccionDAO(Context context){
        this.context = context;
    }

    public ArrayList<Seccion> parseJSON(JSONArray jsonArray){
        ArrayList<Seccion> lista = new ArrayList<Seccion>();

        try {
            JSONObject jsonObject = null;
            Seccion seccion = null;

            for(int i=0;i<jsonArray.length();i++){
                jsonObject = jsonArray.getJSONObject(i);
                seccion = new Seccion();
                seccion.setIdSeccion(jsonObject.getInt("IdSeccion"));
                seccion.setGradoSeccion(jsonObject.getString("GradoSeccion"));
                seccion.setTurno(jsonObject.getString("Turno"));
                lista.add(seccion);
            }

        } catch (JSONException ex) {
            Toast.makeText(context, "Error JSON: "+ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }

        return lista;
    }
}
