package principal.colegio.android.sol_colegio;

import android.colegio.bean.Apoderado;
import android.colegio.bean.Curso;
import android.colegio.bean.Estudiante;
import android.colegio.bean.MessageDialog;
import android.colegio.bean.Profesor;
import android.colegio.bean.Usuario;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.EstudianteDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    Spinner spAlumno;
    EstudianteDAO objestudiantedao;
    ImageView imgLateral;
    TextView lblNombres;

    private Context context;
    private MessageDialog dialog;

    ArrayList<Estudiante> listaEstudiante;
    ArrayAdapter<Estudiante> adapter;
    private int navigationSelected = -1;
    private boolean estudianteCambiado = true;

    enum Perfil{APODERADO, PROFESOR, DIRECTOR}

    enum AccesoMenu{
        RECORD_ACADEMICO(R.id.nav_record, Perfil.APODERADO),
        CURSOS(R.id.nav_cursos, Perfil.APODERADO),
        PROFESORES(R.id.nav_profesores, Perfil.APODERADO),
        SECCIONES(R.id.nav_secciones, Perfil.PROFESOR),
        REPORTES(R.id.nav_reporte, Perfil.PROFESOR);


        private int id;
        private Perfil perfil;

        private AccesoMenu(int id, Perfil perfil){
            this.id = id;
            this.perfil = perfil;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_principal);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();


        context = this;
        dialog = new MessageDialog(context);

        Usuario usuario = Singleton.getInstance().getUsuario();

        if(usuario!=null && usuario.getTipoUsuario().getDescripTipoUsuario().equalsIgnoreCase("Apoderado")){
            Apoderado apoderado = usuario.getApoderado();

            if (apoderado != null) {
                registrationGCM();
                habilitarMenu(menu, Perfil.APODERADO);
                objestudiantedao = new EstudianteDAO(this);

                TextView lblAlumno = (TextView) navigationView.getHeaderView(0).findViewById(R.id.textViewAlumno);
                spAlumno = (Spinner) navigationView.getHeaderView(0).findViewById(R.id.spAlumnos);
                imgLateral = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imgMenu);
                lblNombres = (TextView) navigationView.getHeaderView(0).findViewById(R.id.lblNombreUsuario);


                lblAlumno.setVisibility(View.VISIBLE);
                spAlumno.setVisibility(View.VISIBLE);
                lblNombres.setVisibility(View.VISIBLE);

                lblNombres.setText(apoderado.getNomApoderado()+" "+apoderado.getApePaternoApoderado());


                String urlFoto = apoderado.getFotoApoderado();
                Rest.descargarImagenPorFTP(urlFoto, getApplicationContext(), imgLateral);
                listarEstudiantePorApoderado(apoderado.getCodApoderado());

                spAlumno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Estudiante estudiante = (Estudiante) parent.getItemAtPosition(position);
                        Singleton.getInstance().setEstudiante(estudiante);
                        estudianteCambiado = true;
                        if(navigationSelected != -1 && navigationSelected<3){
                            setFragment(navigationSelected);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }else{
                Toast.makeText(MenuActivity.this,"El apoderado no cargó correctamente",Toast.LENGTH_SHORT).show();
            }
        }
        else{

            if(usuario!=null && usuario.getTipoUsuario().getDescripTipoUsuario().equalsIgnoreCase("Profesor")){
                Profesor profesor = usuario.getProfesor();

                if(profesor != null){
                    habilitarMenu(menu, Perfil.PROFESOR);
                    imgLateral = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imgMenu);
                    lblNombres = (TextView) navigationView.getHeaderView(0).findViewById(R.id.lblNombreUsuario);

                    lblNombres.setText(profesor.getNomProfesor()+" "+profesor.getApePaternoProfesor()+" "+profesor.getApeMaternoProfesor());
                    String urlFoto = Rest.URL_HTTP_IMAGENES +profesor.getFotoProfesor();
                    Rest.descargarImagen(urlFoto, getApplicationContext(), imgLateral);

                }

            }

            spAlumno = (Spinner) navigationView.getHeaderView(0).findViewById(R.id.spAlumnos);
            spAlumno.setVisibility(View.GONE);
            TextView textView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.textViewAlumno);
            textView.setVisibility(View.GONE);

        }


    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            getSupportActionBar().setTitle("Colegio Visionarios");
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_record) {
            navigationSelected = 0;
            setFragment(0);
        } else if (id == R.id.nav_cursos) {
            navigationSelected = 1;
            setFragment(1);
        } else if (id == R.id.nav_profesores) {
            navigationSelected = 2;
            setFragment(2);
        } else if (id == R.id.nav_configuracion) {
            navigationSelected = 3;
            setFragment(3);
        } else if (id == R.id.nav_acercade) {
            navigationSelected = 4;
            setFragment(4);
        }else if(id == R.id.cerrar_sesion){
            logout();
        }else if(id == R.id.nav_secciones){
            setFragment(5);
        }else if(id == R.id.nav_reporte){
            setFragment(6);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        Singleton.getInstance().logout();
        
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.SESSION, Context.MODE_PRIVATE);//Define shared reference file name
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nombreUsuario", "");
        editor.putString("clave", "");
        editor.putString("codUsuario","");

        editor.commit();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void setFragment(int position) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (position) {
            case 0://record academico
                if(getSupportActionBar().getTitle().equals("Record Academico") && estudianteCambiado==false) return;

                estudianteCambiado=false;
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                RecordAcademicoFragment recordAcademicoFragment = new RecordAcademicoFragment();
                fragmentTransaction.replace(R.id.fragment, recordAcademicoFragment);
                fragmentTransaction.addToBackStack("recordAcademicoFragment");
                fragmentTransaction.commit();
                break;
            case 1://listado de cursos
                if(getSupportActionBar().getTitle().equals("Cursos") && estudianteCambiado==false)return;
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                ConsultarCursoFragment cursoFragment = new ConsultarCursoFragment();
                fragmentTransaction.replace(R.id.fragment, cursoFragment);
                fragmentTransaction.addToBackStack("cursoFragment");
                fragmentTransaction.commit();
                break;
            case 2://listado de profesores
                if(getSupportActionBar().getTitle().equals("Profesores") && estudianteCambiado==false)return;
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                ConsultarProfesorFragment inboxFragment = new ConsultarProfesorFragment();
                fragmentTransaction.replace(R.id.fragment, inboxFragment);
                fragmentTransaction.addToBackStack("inboxFragment");
                fragmentTransaction.commit();
                break;
            case 3://configuracion
               // if(getSupportActionBar().getTitle().equals("Configuracion"))return;
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                ConfiguracionFragment configuracionFragment = new ConfiguracionFragment();
                fragmentTransaction.replace(R.id.fragment, configuracionFragment);
                fragmentTransaction.addToBackStack("configuracionFragment");
                fragmentTransaction.commit();
                break;
            case 4://Acerca de
              //  if(getSupportActionBar().getTitle().equals("Acerca De"))return;
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                AcercaDeFragment acercaDeFragment = new AcercaDeFragment();
                fragmentTransaction.replace(R.id.fragment, acercaDeFragment);
                fragmentTransaction.addToBackStack("acercaDeFragment");
                fragmentTransaction.commit();
                break;
            case 5://Listado de secciones

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                ListadoSeccionFragment listadoSeccionFragment = new ListadoSeccionFragment();
                fragmentTransaction.replace(R.id.fragment, listadoSeccionFragment);
                fragmentTransaction.addToBackStack("listadoSeccionFragment");
                fragmentTransaction.commit();
                break;
            case 6://Reportes
                /*
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                AcercaDeFragment acercaDeFragment = new AcercaDeFragment();
                fragmentTransaction.replace(R.id.fragment, acercaDeFragment);
                fragmentTransaction.commit();*/
                break;
        }
    }


    private void registrationGCM(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                    //Registration success
                    String token = intent.getStringExtra("token");
                   // Toast.makeText(getApplicationContext(), "GCM token:" + token, Toast.LENGTH_LONG).show();
                } else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    //Registration error
                    Toast.makeText(getApplicationContext(), "GCM registration error!!!", Toast.LENGTH_LONG).show();
                } else {
                    //Tobe define
                }
            }
        };

        //Check status of Google play service in device
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }




    public void listarEstudiantePorApoderado(String codApoderado){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/apoderado/listarEstudiantes");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codApoderado", codApoderado);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando estudiantes");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        if(response!=null){
                            if(response.length()>0){
                                listaEstudiante = objestudiantedao.parseJSON(response);
                                adapter = new ArrayAdapter<Estudiante>(context, android.R.layout.simple_spinner_item, listaEstudiante);
                                spAlumno.setAdapter(adapter);
                                notificacionRecibida();
                            }
                        }
                    }
                },
                new ErrorVolley(context, dialog)
        );
        VolleyRequest.getInstance(context).addToRequestQueue(request);

    }


    private void notificacionRecibida(){
        Intent intent = getIntent();
        if(intent!=null && intent.getAction()!=null && intent.getAction().equals(GCMPushReceiverService.PUSH_RECEIVE)){
            FragmentManager fragmentManager;
            FragmentTransaction fragmentTransaction;

            Curso curso = (Curso)intent.getSerializableExtra("curso");
            String codEstudiante = intent.getExtras().getString("codEstudiante");
            byte trimestre = intent.getExtras().getByte("trimestre");
            int position = buscarEstudiante(codEstudiante);
            spAlumno.setSelection(position);
            String codTipoEvaluacion = intent.getExtras().getString("codTipoEvaluacion");

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            ContenedorCursoFragment contenedorCursoFragment = new ContenedorCursoFragment();

            Bundle bundle = new Bundle();

            bundle.putSerializable("curso",curso);
            bundle.putByte("trimestre", trimestre);
            bundle.putString("codTipoEvaluacion", codTipoEvaluacion);
            contenedorCursoFragment.setArguments(bundle);

            fragmentTransaction.replace(R.id.fragment, contenedorCursoFragment);
            fragmentTransaction.addToBackStack("contenedorCursoFragment");
            fragmentTransaction.commit();

            Toast.makeText(this, "Notificacion recibida", Toast.LENGTH_SHORT).show();
        }
    }

    private int buscarEstudiante(String codEstudiante){
        int position = -1;
        int i=0;
        for(Estudiante estudiante:listaEstudiante){
            if(estudiante.getCodEstudiante().equals(codEstudiante)){
                position=i;
                break;
            }
            i++;
        }

        return position;
    }

    private void habilitarMenu(Menu menu, Perfil perfil){
        MenuItem menuItem = null;
        for(int i=0; i<menu.size(); i++){
            menuItem = menu.getItem(i);
            int id = menuItem.getItemId();

            for(AccesoMenu accesoMenu : AccesoMenu.values()){
                if(accesoMenu.id == id  && accesoMenu.perfil == perfil ){
                    menuItem.setVisible(true);
                    break;
                }
            }
        }
    }

}