package principal.colegio.android.sol_colegio;

import android.colegio.adapter.RecordAcademicoAdapter;
import android.colegio.bean.Estudiante;
import android.colegio.bean.MessageDialog;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.RecordAcademicoDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;


/**
 * Created by hramos on 6/04/2016.
 */
public class RecordAcademicoFragment extends Fragment{

    private ListView lstRecordTrimestral;
    private RecordAcademicoDAO recorddao;
    private MessageDialog dialog;
    private Context context;
    RecordAcademicoAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      return inflater.inflate(R.layout.record_academico, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Record Academico");
        View v = getView();
        lstRecordTrimestral = (ListView) v.findViewById(R.id.lstRecordTrimestral);

        context = v.getContext();
        dialog = new MessageDialog(context);

        recorddao = new RecordAcademicoDAO(getContext());

        if (Singleton.getInstance().getEstudiante() != null) {
            Estudiante estudiante = Singleton.getInstance().getEstudiante();
            mostrarRecordAcademico(estudiante.getCodEstudiante());
        }


    }


    public void mostrarRecordAcademico(String codEstudiante){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/evaluacion/recordTotal");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codEstudiante", codEstudiante);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando record academico");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        if(response!=null){
                            if(response.length()>0){
                                ArrayList<Hashtable<String, Object>> lista = recorddao.parseJSON(response);
                                adapter = new RecordAcademicoAdapter(context, lista);
                                lstRecordTrimestral.setAdapter(adapter);
                            }
                        }
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }
}