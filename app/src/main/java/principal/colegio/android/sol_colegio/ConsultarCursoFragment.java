package principal.colegio.android.sol_colegio;

import android.colegio.adapter.CursoAdapter;
import android.colegio.bean.Curso;
import android.colegio.bean.Estudiante;
import android.colegio.bean.MessageDialog;
import android.colegio.dao.CursoDAO;
import android.colegio.dao.ErrorVolley;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by hramos on 6/04/2016.
 */
public class ConsultarCursoFragment extends Fragment implements  CursoAdapter.OnItemClickListener{

    private CursoDAO daoCurso;
    private MenuActivity menuActivity;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private MessageDialog dialog;
    private Context context;
    private CursoAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.curso_recycler, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        menuActivity = (MenuActivity) getActivity();
        menuActivity.getSupportActionBar().setTitle("Cursos");
        View v = getView();
        context = v.getContext();
        dialog = new MessageDialog(context);

        recycler = (RecyclerView) v.findViewById(R.id.recyclerCurso);
        recycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(v.getContext());
        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        daoCurso = new CursoDAO(v.getContext());

        if (Singleton.getInstance().getEstudiante() != null) {
            Estudiante estudiante = Singleton.getInstance().getEstudiante();
            listarCursos(estudiante.getCodEstudiante());

        }
    }


    @Override
    public void onClick(RecyclerView.ViewHolder v) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;

        fragmentManager = menuActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        ContenedorCursoFragment contenedorCursoFragment = new ContenedorCursoFragment();
        Bundle bundle = new Bundle();

        CursoAdapter adapter = (CursoAdapter) recycler.getAdapter();
        int position = v.getAdapterPosition();
        Curso curso =  adapter.getCurso(position);

        bundle.putSerializable("curso",curso);
        contenedorCursoFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment, contenedorCursoFragment);
        fragmentTransaction.addToBackStack("contenedorCursoFragment");
        fragmentTransaction.commit();
    }


    public void listarCursos(String codEstudiante)
    {
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/estudiante/listarCursos");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codEstudiante", codEstudiante);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando cursos");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        ArrayList<Curso> listaCurso = daoCurso.parseJSON(response);
                        adapter = new CursoAdapter(listaCurso);
                        adapter.setOnItemClickListener(ConsultarCursoFragment.this);
                        recycler.setAdapter(adapter);
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }
}
