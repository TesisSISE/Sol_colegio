package principal.colegio.android.sol_colegio;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.colegio.bean.Curso;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by NgocTri on 4/9/2016.
 */
public class GCMPushReceiverService extends GcmListenerService {
    public static final String PUSH_RECEIVE="recibido";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String codUsuario = data.getString("codusuario");

        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.SESSION, Context.MODE_PRIVATE);//Define shared reference file name
        String ultimoUsuario = sharedPreferences.getString("codUsuario","");


        if(codUsuario!=null && codUsuario.equals(ultimoUsuario) && !codUsuario.isEmpty()){
            sendNotification(data);
        }


        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

    }



    private void sendNotification(String message) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(PUSH_RECEIVE);
        int requestCode = 0;//Your request code
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        //Setup notification
        //Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Build notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_visionarios)
                .setTicker(getString(R.string.app_name))
                .setWhen(0)
                .setContentTitle("Registro de Notas")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent)
                ;

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build()); //0 = ID of notification
    }

    private void sendNotification(Bundle data) {
        String message = data.getString("message");
        String codCurso = data.getString("codcurso");
        String descripCurso = data.getString("descripcurso");
        String codTipoEvaluacion = data.getString("codTipoEvaluacion");
        String codEstudiante = data.getString("codEstudiante");
        byte trimestre = Byte.parseByte(data.getString("trimestre"));
        Curso curso = new Curso(codCurso, descripCurso);

        Intent intent = new Intent(this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(PUSH_RECEIVE);

        intent.putExtra("curso", curso);
        intent.putExtra("codEstudiante", codEstudiante);
        intent.putExtra("trimestre", trimestre);
        intent.putExtra("codTipoEvaluacion", codTipoEvaluacion);

        int requestCode = 0;//Your request code
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        //Setup notification
        //Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Build notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_visionarios)
                .setTicker(getString(R.string.app_name))
                .setWhen(0)
                .setContentTitle("Registro de Notas")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pendingIntent)
                ;

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build()); //0 = ID of notification
    }
}
