package principal.colegio.android.sol_colegio;

import android.app.Activity;
import android.colegio.bean.Apoderado;
import android.colegio.bean.MessageDialog;
import android.colegio.bean.Usuario;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.UsuarioDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hramos on 1/04/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    public static final String SESSION="session";

    private CardView btnLogin;
    private EditText txtUsuario, txtClave;
    private UsuarioDAO usuariodao;


    private MessageDialog dialog;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        dialog = new MessageDialog(context);
        usuariodao =  new UsuarioDAO(this);
        iniciarSesionAutomaticamente();

        setContentView(R.layout.login);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        txtUsuario = (EditText)findViewById(R.id.txtUsuarioLogin);
        txtClave = (EditText)findViewById(R.id.txtContrasenaLogin);


        btnLogin = (CardView) findViewById(R.id.btnIngresar);
        btnLogin.setOnClickListener(this);
    }

    private void iniciarSesionAutomaticamente(){
        SharedPreferences sharedPreferences = getSharedPreferences(SESSION, Context.MODE_PRIVATE);//Define shared reference file name

        String nombreUsuario = sharedPreferences.getString("nombreUsuario","");
        String clave = sharedPreferences.getString("clave","");

        if(!nombreUsuario.isEmpty() & !clave.isEmpty()){
            login(nombreUsuario, clave);
        }
    }

    public void guardarSesion(String nombreUsuario, String clave, String codUsuario) {
        SharedPreferences sharedPreferences = getSharedPreferences(SESSION, Context.MODE_PRIVATE);//Define shared reference file name
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nombreUsuario", nombreUsuario);
        editor.putString("clave", clave);
        editor.putString("codUsuario", codUsuario);

        editor.commit();
    }


    @Override
    public void onClick(View v) {
        if (v == btnLogin) {

            if(txtUsuario.getText().toString().trim().isEmpty()){
                txtUsuario.setError("Ingrese un usuario");
                txtUsuario.requestFocus();
                return;
            }
            else if(txtClave.getText().toString().trim().isEmpty()){
                txtClave.setError("Ingrese una clave");
                txtClave.requestFocus();
                return;
            }

            String usuario = txtUsuario.getText().toString();
            String clave = txtClave.getText().toString();
            login(usuario,clave);
        }
    }


    public void login(final String nombreUsuario, final String clave){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/usuario/login");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("nombreUsuario", nombreUsuario);
        parametros.put("clave", clave);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Validando credenciales","Espere unos segundos...");
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                rutaCompleta.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.closeProgressBar();

                        if (response != null) {

                            if(response.length()>0){
                                Usuario usuario = usuariodao.parseJSON(response);

                                if (usuario != null) {

                                    guardarSesion(nombreUsuario, clave, usuario.getCodUsuario());
                                    if (usuario.getTipoUsuario().getDescripTipoUsuario().equalsIgnoreCase("Profesor")) {
                                        Toast.makeText(context, "Bienvenido Profesor " + usuario.getNomUsuario(),
                                                Toast.LENGTH_LONG).show();
                                    } else if (usuario.getTipoUsuario().getDescripTipoUsuario().equalsIgnoreCase("Director")) {
                                        Toast.makeText(context, "Bienvenido Director " + usuario.getNomUsuario(),
                                                Toast.LENGTH_LONG).show();
                                    } else if (usuario.getTipoUsuario().getDescripTipoUsuario().equalsIgnoreCase("Apoderado")) {
                                        Apoderado apoderado = usuario.getApoderado();
                                        Toast.makeText(context, "Bienvenido " + apoderado.getNomApoderado(),
                                                Toast.LENGTH_LONG).show();
                                    }


                                    Singleton.getInstance().setUsuario(usuario);


                                    Intent intent = new Intent(context, MenuActivity.class);
                                    context.startActivity(intent);
                                    LoginActivity.this.finish();
                                } else {
                                    Toast.makeText(context, "Usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(context, "Usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                },
                new ErrorVolley(context, dialog)


        );
        VolleyRequest.getInstance(context).addToRequestQueue(request);

    }


}