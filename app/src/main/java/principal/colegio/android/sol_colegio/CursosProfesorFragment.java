package principal.colegio.android.sol_colegio;

import android.support.v4.app.Fragment;
import android.colegio.adapter.CursoAdapter;
import android.colegio.bean.Curso;
import android.colegio.bean.MessageDialog;
import android.colegio.bean.Profesor;
import android.colegio.bean.Seccion;
import android.colegio.dao.CursoDAO;
import android.colegio.dao.ErrorVolley;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 33.1 on 03/06/2016.
 */
public class CursosProfesorFragment extends Fragment implements  CursoAdapter.OnItemClickListener{

    private CursoDAO daoCurso;
    private MenuActivity menuActivity;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private MessageDialog dialog;
    private Context context;
    private CursoAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.curso_recycler, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        menuActivity = (MenuActivity) getActivity();
        Bundle bundle = getArguments();
        Seccion seccion = (Seccion) bundle.getSerializable("seccion");
        menuActivity.getSupportActionBar().setTitle("Cursos - "+seccion.getGradoSeccion());
        View v = getView();
        context = v.getContext();
        dialog = new MessageDialog(context);

        recycler = (RecyclerView) v.findViewById(R.id.recyclerCurso);
        recycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(v.getContext());
        recycler.setLayoutManager(layoutManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

        daoCurso = new CursoDAO(v.getContext());

        if(Singleton.getInstance().getUsuario().getProfesor()!=null){
            Profesor profesor = Singleton.getInstance().getUsuario().getProfesor();

            String idSeccion = Integer.toString(seccion.getIdSeccion());
            listarCursos(idSeccion, profesor.getCodProfesor());
        }


    }

    @Override
    public void onClick(RecyclerView.ViewHolder v) {
        Toast.makeText(context, "Presionado", Toast.LENGTH_SHORT).show();
    }

    public void listarCursos(String idSeccion, String codProfesor)
    {
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/profesor/listarCursosDeSeccion");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("idSeccion", idSeccion);
        parametros.put("codProfesor", codProfesor);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando cursos");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        ArrayList<Curso> listaCurso = daoCurso.parseJSON(response);
                        adapter = new CursoAdapter(listaCurso);
                        adapter.setOnItemClickListener(CursosProfesorFragment.this);
                        recycler.setAdapter(adapter);
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }
}
