package principal.colegio.android.sol_colegio;

import android.app.AlertDialog;
import android.colegio.adapter.ProfesorAdapter;
import android.colegio.bean.Estudiante;
import android.colegio.bean.MessageDialog;
import android.colegio.bean.Profesor;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.ProfesorDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hramos on 6/04/2016.
 */
public class ConsultarProfesorFragment extends Fragment implements AdapterView.OnItemClickListener{
    Context context;
    TextView lblNombre, lblApellido;
    ListView lstProfesor;

    ProfesorAdapter adapter;
    ProfesorDAO daoProfesor;

    ArrayList<Profesor> listaProfesores;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.listar_profesor_alumno, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Profesores");
        View v = getView();
        lblNombre = (TextView)v.findViewById(R.id.LBLNOMBREPROFESOR);
        lblApellido = (TextView)v.findViewById(R.id.LBLNOMBREPROFESOR);
        lstProfesor = (ListView) v.findViewById(R.id.lstProfesor);

        lstProfesor.setOnItemClickListener(this);
        context = v.getContext();
        daoProfesor = new ProfesorDAO(context);


        if(Singleton.getInstance().getEstudiante()!=null){
            Estudiante estudiante = Singleton.getInstance().getEstudiante();
            listarProfesores(estudiante.getCodEstudiante());
        }
    }

    public void listarProfesores(String codEstudiante){
        final MessageDialog dialog = new MessageDialog(context);
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/estudiante/listarProfesores");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codEstudiante", codEstudiante);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando profesores");
        StringRequest request = new StringRequest(Request.Method.GET, rutaCompleta.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                    if(isAdded()){
                        dialog.closeProgressBar();
                        listaProfesores = daoProfesor.parseJSON(response);
                        adapter = new ProfesorAdapter(context, listaProfesores);
                        lstProfesor.setAdapter(adapter);
                    }
            }
        }, new ErrorVolley(context, dialog));
        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Profesor profesor = (Profesor) adapter.getItem(position);

        if(profesor!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Información de contacto");
            String mensaje = "Correo: "+profesor.getEmailProfesor()+
                    "\nTelefono: "+profesor.getTelfProfesor();
            builder.setMessage(mensaje);
            builder.setPositiveButton("OK", null);
            builder.show();
        }

    }
}