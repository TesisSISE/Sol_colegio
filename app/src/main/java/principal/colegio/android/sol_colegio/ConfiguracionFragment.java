package principal.colegio.android.sol_colegio;

import android.colegio.bean.MessageDialog;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.UsuarioDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hramos on 7/04/2016.
 */
public class ConfiguracionFragment extends Fragment implements View.OnClickListener{
    private TextView txtClaveActual, txtClaveNueva, txtRepetirNuevaClave;
    private Button btnGuardarCambios, btnCancelar;
    private UsuarioDAO dao;
    private Context context;
    private MessageDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.configuracion, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MenuActivity) getActivity()).getSupportActionBar().setTitle("Configuracion");

        View v = getView();
        txtClaveActual = (TextView) v.findViewById(R.id.txtClaveActual);
        txtClaveNueva = (TextView) v.findViewById(R.id.txtClaveNueva);
        txtRepetirNuevaClave = (TextView) v.findViewById(R.id.txtRepetirClaveNueva);
        btnGuardarCambios = (Button) v.findViewById(R.id.btnGuardarCambios);
        btnCancelar = (Button) v.findViewById(R.id.btnCancelar);

        btnGuardarCambios.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        dao = new UsuarioDAO(v.getContext());
        context = v.getContext();
        dialog = new MessageDialog(context);
    }

    @Override
    public void onClick(View v) {
        if( v == btnGuardarCambios){
            guardarCambios(v);
        }
        else if( v == btnCancelar){
            limpiarCampos(v);
        }
    }

    private void guardarCambios(View v){

        if(txtClaveActual.getText().toString().trim().isEmpty()){
            txtClaveActual.setError("Ingrese su contraseña actual");
            txtClaveActual.requestFocus();
            return;
        }

        if(txtClaveNueva.getText().toString().trim().isEmpty()){
            txtClaveNueva.setError("Ingrese una nueva contraseña");
            txtClaveNueva.requestFocus();
            return;
        }

        if(txtRepetirNuevaClave.getText().toString().trim().isEmpty()){
            txtRepetirNuevaClave.setError("Repita su nueva contraseña");
            txtRepetirNuevaClave.requestFocus();
            return;
        }

        String claveActual = Singleton.getInstance().getUsuario().getClaveUsuario();
        String claveActualIngresada = txtClaveActual.getText().toString();
        String nuevaClave = txtClaveNueva.getText().toString();
        String nuevaClaveRepetida = txtRepetirNuevaClave.getText().toString();

        if(!nuevaClave.equals(nuevaClaveRepetida)){
            Toast.makeText(v.getContext(), "Las nuevas claves no coinciden", Toast.LENGTH_SHORT).show();
        }
        else if(!claveActual.equals(claveActualIngresada)){
            txtClaveActual.setError("Tu contraseña está incorrecta");
            txtClaveActual.requestFocus();
        }else{
            cambiarClave(claveActualIngresada, nuevaClave);
            limpiarCampos(v);
        }


    }

    private void limpiarCampos(View v){
        txtClaveActual.setText("");
        txtClaveNueva.setText("");
        txtRepetirNuevaClave.setText("");
    }

    public void cambiarClave(String clave, String nuevaClave){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/usuario/cambiarClave");

        Map<String,String> parametros = new HashMap<>();
        String codUsuario = Singleton.getInstance().getUsuario().getCodUsuario();
        parametros.put("codUsuario", codUsuario);
        parametros.put("claveUsuario", clave);
        parametros.put("nuevaClaveusuario", nuevaClave);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Validando contraseña","Espere unos segundos...");

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.PUT,
                rutaCompleta.toString(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.closeProgressBar();
                        if (response != null) {
                            String resultado = dao.getResultado(response);
                        }
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }
}