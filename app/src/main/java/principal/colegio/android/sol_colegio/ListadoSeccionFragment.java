package principal.colegio.android.sol_colegio;

import android.support.v4.app.Fragment;
import android.colegio.adapter.SeccionAdapter;
import android.colegio.bean.MessageDialog;
import android.colegio.bean.Profesor;
import android.colegio.bean.Seccion;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.SeccionDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 33.1 on 03/06/2016.
 */
public class ListadoSeccionFragment extends Fragment implements SeccionAdapter.OnItemClickListener{
    private SeccionDAO daoSeccion;
    private MenuActivity menuActivity;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;
    private MessageDialog dialog;
    private SeccionAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.seccion_recycler, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View v = getView();
        menuActivity = (MenuActivity) getActivity();
        menuActivity.getSupportActionBar().setTitle("Salones");
        context = v.getContext();
        dialog = new MessageDialog(context);

        recycler = (RecyclerView) v.findViewById(R.id.recyclerSeccion);
        recycler.setHasFixedSize(true);

        //layoutManager = new LinearLayoutManager(v.getContext());
        recycler.setLayoutManager(new GridLayoutManager(context, 2));
        recycler.setItemAnimator(new DefaultItemAnimator());

        daoSeccion = new SeccionDAO(context);

        if(Singleton.getInstance().getUsuario().getProfesor()!=null){
            Profesor profesor = Singleton.getInstance().getUsuario().getProfesor();
            listarSecciones(profesor.getCodProfesor());
        }
    }


    public void listarSecciones(String codProfesor){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/profesor/listarSecciones");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codProfesor", codProfesor);

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando Secciones");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        dialog.closeProgressBar();
                        ArrayList<Seccion> lista = daoSeccion.parseJSON(response);
                        adapter = new SeccionAdapter(lista);
                        adapter.setOnItemClickListener(ListadoSeccionFragment.this);
                        recycler.setAdapter(adapter);

                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }

    @Override
    public void onClick(RecyclerView.ViewHolder v) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;

        fragmentManager = menuActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        CursosProfesorFragment cursosProfesorFragment = new CursosProfesorFragment();

        Bundle bundle = new Bundle();

        int position = v.getAdapterPosition();
        Seccion seccion = adapter.getSeccion(position);
        bundle.putSerializable("seccion", seccion);


        cursosProfesorFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.fragment, cursosProfesorFragment);
        fragmentTransaction.addToBackStack("cursosProfesorFragment");
        fragmentTransaction.commit();

    }
}
