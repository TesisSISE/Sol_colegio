package principal.colegio.android.sol_colegio;

import android.colegio.bean.Curso;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 33.1 on 26/04/2016.
 */
public class ContenedorCursoFragment extends Fragment{
    private AppBarLayout appBar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Curso curso;

    private CriterioNotaFragment trimestreUno;
    private CriterioNotaFragment trimestreDos;
    private CriterioNotaFragment trimestreTres;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_contenedor_nota, container, false);
        if (savedInstanceState == null) {
            insertarTabs(container);

            viewPager = (ViewPager) view.findViewById(R.id.view_pager_curso);
            poblarViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
            seleccionarTrimestre(tabLayout);
        }

        return view;
    }

    private void seleccionarTrimestre(TabLayout tabLayout) {
        Bundle bundle = getArguments();

        if(bundle!=null){
            byte trimestre = bundle.getByte("trimestre");

            if(trimestre!=0){
                tabLayout.getTabAt(trimestre-1).select();
                //una vez seleccionado el trimestre mostrar el dialog donde se encuentra la nota
                String codTipoEvaluacion = bundle.getString("codTipoEvaluacion");
                Bundle datos = null;
                switch (trimestre){
                    case 1:
                        datos =trimestreUno.getArguments();
                        datos.putString("codTipoEvaluacion", codTipoEvaluacion);
                        break;
                    case 2:
                        datos =trimestreDos.getArguments();
                        datos.putString("codTipoEvaluacion", codTipoEvaluacion);
                        break;
                    case 3:
                        datos =trimestreTres.getArguments();
                        datos.putString("codTipoEvaluacion", codTipoEvaluacion);
                        break;
                }


            }

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MenuActivity) getActivity()).getSupportActionBar().setTitle(curso.getDescripCurso());

    }

    private void insertarTabs(ViewGroup container) {
        View padre = (View) container.getParent().getParent();
        appBar = (AppBarLayout) padre.findViewById(R.id.appbar);

        if (appBar != null) {
            tabLayout = new TabLayout(padre.getContext());
            tabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));
            appBar.addView(tabLayout);
        }
    }


    private void poblarViewPager(ViewPager viewPager) {
        AdaptadorSecciones adapter = new AdaptadorSecciones(getFragmentManager());

        trimestreUno = new CriterioNotaFragment();
        trimestreDos = new CriterioNotaFragment();
        trimestreTres = new CriterioNotaFragment();

        //trimestreUno.setOnItemClickListener(this);
        //trimestreDos.setOnItemClickListener(this);
        //trimestreTres.setOnItemClickListener(this);

        Bundle bundle = getArguments();
        curso = (Curso) bundle.getSerializable("curso");

        Bundle arg1 = new Bundle();
        arg1.putInt("trimestre",1);
        arg1.putString("codCurso",curso.getCodCurso());
        trimestreUno.setArguments(arg1);

         Bundle arg2 = new Bundle();
         arg2.putInt("trimestre",2);
         arg2.putString("codCurso",curso.getCodCurso());
         trimestreDos.setArguments(arg2);

         Bundle arg3 = new Bundle();
         arg3.putInt("trimestre",3);
         arg3.putString("codCurso",curso.getCodCurso());
         trimestreTres.setArguments(arg3);


        adapter.addFragment(trimestreUno,"1er Trimestre" );
        adapter.addFragment(trimestreDos,"2do Trimestre" );
        adapter.addFragment(trimestreTres,"3er Trimestre" );

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (appBar != null) {
            appBar.removeView(tabLayout);
        }

    }

    /*
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.lstNotaTrimestre:
                 CriterioNota notaTrimestre = (CriterioNota) ((ListView)parent).getAdapter().getItem(position);
                 //Toast.makeText(getContext(), Double.toString(notaTrimestre.getPromedio()), Toast.LENGTH_SHORT).show();
                CriterioNotaDAO dao = new CriterioNotaDAO(view.getContext());
                String codEstudiante = Singleton.getInstance().getEstudiante().getCodEstudiante();

               // dao.mostrarDetalleNota(codEstudiante, curso.getCodCurso(), );
                break;
        }
    }
*/

    /**
     * Un {@link FragmentStatePagerAdapter} que gestiona las secciones, fragmentos y
     * títulos de las pestañas
     */
    public class AdaptadorSecciones extends FragmentStatePagerAdapter {
        private final List<Fragment> fragmentos = new ArrayList<>();
        private final List<String> titulosFragmentos = new ArrayList<>();

        public AdaptadorSecciones(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return fragmentos.get(position);
        }

        @Override
        public int getCount() {
            return fragmentos.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            fragmentos.add(fragment);
            titulosFragmentos.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titulosFragmentos.get(position);
        }
    }
}
