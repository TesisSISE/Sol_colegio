package principal.colegio.android.sol_colegio;


/**
 * Created by 33.1 on 01/05/2016.
 */

import android.app.IntentService;
import android.colegio.bean.Usuario;
import android.colegio.dao.ErrorVolley;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;


/**
 * Created by NgocTri on 4/8/2016.
 */
public class GCMRegistrationIntentService extends IntentService {
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    public static final String TAG = "GCMTOKEN";
    public static final String TAG_CODIGOS = "GCMCodigoUsuarios";
    public GCMRegistrationIntentService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(Singleton.getInstance().getUsuario()!=null && Singleton.getInstance().getUsuario().getApoderado()!=null){
            registerGCM();
        }
    }

    private void registerGCM() {
        SharedPreferences sharedPreferences = getSharedPreferences("GCM", Context.MODE_PRIVATE);//Define shared reference file name
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Intent registrationComplete = null;
        String token = null;
        Set<String> listaCodigoUsuarios = null;
        try {
            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.w("GCMRegIntentService", "token:" + token);

            String idMovil = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.w("GCMRegistrationService","id movil:"+ idMovil);
            //notify to UI that registration complete success
            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            registrationComplete.putExtra("token", token);

            String oldToken = sharedPreferences.getString(TAG, "");//Return "" when error or key not exists
            listaCodigoUsuarios = sharedPreferences.getStringSet(TAG_CODIGOS, new HashSet<String>());

            //Only request to save token when token is new
            if(!token.isEmpty() && !oldToken.equals(token)) {
                Log.w("GCMRegistrationService", "New token");

                saveTokenToServer(token);
                //Save new token to shared reference
                registrarCodigoUsuario(listaCodigoUsuarios, editor);
                editor.putString(TAG, token);
                editor.commit();
            } else {
                Log.w("GCMRegistrationService", "Old token");
                String codUsuario = Singleton.getInstance().getUsuario().getCodUsuario();

                if(!isCodigoRegistrado(codUsuario, listaCodigoUsuarios)){
                    registrarCodigoUsuario(listaCodigoUsuarios, editor);
                    editor.commit();
                    saveTokenToServer(token);
                    Log.w("GCMRegistrationService", "Old token Registrado en un nuevo usuario!!!");
                }
            }



        } catch (Exception e) {
            Log.w("GCMRegIntentService", "Registration error");
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }
        //Send broadcast
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void saveTokenToServer(String token) {
        String url= Rest.URL_SERVICE_BASE +"/movil/registrar";
        String idMovil = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.w("GCMRegistrationService", idMovil);
        Usuario usuario = Singleton.getInstance().getUsuario();
        String codUsuario = usuario.getCodUsuario();
        String json = "{\"idMovil\":\""+idMovil+"\", \"codUsuario\":\""+codUsuario+"\", \"token\":\""+token+"\"}";

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                url,
                json,
                new Response.Listener<JSONObject>(){

                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null){
                            Log.d("GCMRegistration", response.toString());
                        }else{
                            Log.d("GCMRegistration", response.toString());
                        }
                    }
                }
                ,
                new ErrorVolley(getApplicationContext())

        );
        VolleyRequest.getInstance(getApplicationContext()).addToRequestQueue(request);

    }

    private boolean isCodigoRegistrado(String codUsuario, Set<String> listaCodigos){
        boolean encontrado=false;

        for(String codigo: listaCodigos){
            if(codUsuario.equals(codigo)){
                encontrado=true;
                break;
            }
        }
        return encontrado;
    }

    private void registrarCodigoUsuario(Set<String> listaCodigos, SharedPreferences.Editor editor){
        String codUsuario = Singleton.getInstance().getUsuario().getCodUsuario();

        if(!isCodigoRegistrado(codUsuario, listaCodigos)){
            listaCodigos.add(codUsuario);
            editor.putStringSet(TAG_CODIGOS, listaCodigos);
        }
    }
}
