package principal.colegio.android.sol_colegio;

import android.app.AlertDialog;
import android.colegio.adapter.CriterioNotaAdapter;
import android.colegio.adapter.EvaluacionAdapter;
import android.colegio.bean.CriterioNota;
import android.colegio.bean.Estudiante;
import android.colegio.bean.Evaluacion;
import android.colegio.bean.MessageDialog;
import android.colegio.dao.CriterioNotaDAO;
import android.colegio.dao.ErrorVolley;
import android.colegio.dao.EvaluacionDAO;
import android.colegio.util.Rest;
import android.colegio.util.Singleton;
import android.colegio.util.VolleyRequest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by 33.1 on 26/04/2016.
 */
public class CriterioNotaFragment extends Fragment implements AdapterView.OnItemClickListener{
    private ListView lstNota;
    private CriterioNotaDAO criterioNotaDAO;
    private EvaluacionDAO evaluacionDAO;

    private String codCurso;
    private int trimestre;

    private MessageDialog dialog;
    private Context context;

    private CriterioNotaAdapter criterioNotaAdapter;
    private DecimalFormat formatter;

    private ArrayList<CriterioNota> listaCriterioNotas;


    private LayoutInflater layoutInflater;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private EvaluacionAdapter evaluacionAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.layout_trimestrenota, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View v = getView();
        context = v.getContext();
        dialog = new MessageDialog(context);
        formatter=new DecimalFormat("##00");

        lstNota = (ListView) v.findViewById(R.id.lstNotaTrimestre);

        Bundle bundle = getArguments();
        trimestre = bundle.getInt("trimestre");
        codCurso = bundle.getString("codCurso");
        criterioNotaDAO = new CriterioNotaDAO(context);
        evaluacionDAO = new EvaluacionDAO(context);
        layoutInflater = getLayoutInflater(savedInstanceState);
        Estudiante estudiante = Singleton.getInstance().getEstudiante();
        mostrarNotaTrimestre(estudiante.getCodEstudiante(), codCurso, trimestre);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CriterioNota criterioNota = (CriterioNota) ((ListView)parent).getAdapter().getItem(position);
        CriterioNotaDAO dao = new CriterioNotaDAO(view.getContext());
        String codEstudiante = Singleton.getInstance().getEstudiante().getCodEstudiante();
        mostrarDetalleNota(codEstudiante, codCurso, trimestre, criterioNota.getCodTipoEvaluacion(), criterioNota.getPromedio());


    }

    public void mostrarNota(){
        String codTipoEvaluacion = getArguments().getString("codTipoEvaluacion");
        if(codTipoEvaluacion!=null && !codTipoEvaluacion.isEmpty()){
            int position = buscarCriterioNota(codTipoEvaluacion);
            CriterioNota criterioNota = listaCriterioNotas.get(position);
            CriterioNotaDAO dao = new CriterioNotaDAO(getContext());
            String codEstudiante = Singleton.getInstance().getEstudiante().getCodEstudiante();
            mostrarDetalleNota(codEstudiante, codCurso, trimestre, criterioNota.getCodTipoEvaluacion(), criterioNota.getPromedio());
        }

    }


    public void mostrarNotaTrimestre(String codEstudiante, String codCurso, int trimestre){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/evaluacion/promedioPorCriterioTrimestral");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codEstudiante", codEstudiante);
        parametros.put("codCurso", codCurso);
        parametros.put("trimestre", Integer.toString(trimestre));

        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando notas de curso");

        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        if (response != null) {
                            if (response.length() > 0) {
                                listaCriterioNotas = criterioNotaDAO.parseJSON(response);
                                criterioNotaAdapter = new CriterioNotaAdapter(context, listaCriterioNotas);                                lstNota.setAdapter(criterioNotaAdapter);
                                lstNota.setOnItemClickListener(CriterioNotaFragment.this);
                                mostrarNota();
                            }
                        }
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);
    }


    public void mostrarDetalleNota(String codEstudiante, String codCurso, int trimestre, String codTipoEvaluacion, final double promedio){
        StringBuilder rutaCompleta = new StringBuilder(Rest.URL_SERVICE_BASE +"/evaluacion/detalleNota");

        Map<String,String> parametros = new HashMap<>();
        parametros.put("codEstudiante", codEstudiante);
        parametros.put("codCurso", codCurso);
        parametros.put("trimestre", Integer.toString(trimestre));
        parametros.put("codTipoEvaluacion", codTipoEvaluacion);
        Rest.addParametersToPath(rutaCompleta, parametros);

        dialog.showProgressBar("Cargar","Cargando detalle de nota");
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                rutaCompleta.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.closeProgressBar();
                        if (response != null) {
                            if (response.length() > 0) {
                                ArrayList<Evaluacion> lista = evaluacionDAO.parse(response);

                                View v = layoutInflater.inflate(R.layout.nota_recycler, null);
                                recycler = (RecyclerView) v.findViewById(R.id.recyclerNota);
                                recycler.setHasFixedSize(true);

                                layoutManager = new LinearLayoutManager(v.getContext());
                                recycler.setLayoutManager(layoutManager);
                                recycler.setItemAnimator(new DefaultItemAnimator());

                                evaluacionAdapter = new EvaluacionAdapter(lista);
                                recycler.setAdapter(evaluacionAdapter);


                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Detalle de Nota");
                                builder.setView(v);
                                builder.setPositiveButton("OK",null);
                                builder.show();
                            }
                        }
                    }
                },
                new ErrorVolley(context, dialog)
        );

        VolleyRequest.getInstance(context).addToRequestQueue(request);

    }

    private int buscarCriterioNota(String codTipoEvaluacion){
        int encontrado = -1;
        int i=0;

        for(CriterioNota criterioNota : listaCriterioNotas){

            if(criterioNota.getCodTipoEvaluacion().equals(codTipoEvaluacion)){
                encontrado=i;
                break;
            }
            i++;
        }


        return encontrado;
    }
}
